from dataclasses import dataclass, field
from datetime import datetime


@dataclass
class SimpleArticle:
    doi: str = "no info"
    unstructured: str = "no info"

@dataclass
class Article(SimpleArticle):
    title: str = "no info"
    publisher: str = "no info"
    date: datetime = datetime.fromtimestamp(0)
    cited: int = -1
    references: list[SimpleArticle] = field(default_factory= lambda: []) # list of doi's
