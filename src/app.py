#! /usr/bin/env python3

from Articles import Article, SimpleArticle
from CrossRefAPI import CrossRefAPI
from argparse import ArgumentParser
from pathlib import Path
import csv
import re



clearLine = " " * 50 + "\r"

def isDOI(DOI: str) -> bool:
    regex = r"^10\.\d{4,9}/[-._;()/:a-zA-Z0-9]+$"
    
    return re.match(regex, DOI) is not None

def saveCSV(path: Path, data: tuple[tuple[str, int]]) -> None:
    with open(path, "w", newline="", encoding="utf-8") as csvfile:
        spamwriter = csv.writer(csvfile, delimiter=",")
        
        spamwriter.writerow(["doi", "title", "citised"])
        
        for item in data:
            if all([i != "no info" for i in item]):
                spamwriter.writerow(item)
    
    
def main() -> int:
    parser = ArgumentParser(prog="ScienceParser",
                            description="Program for getting information about scientific articles by their DOI")
    
    parser.add_argument('-i', '--input', type=str, help='Input file with DOI\'s from a new line or DOI like "DOI: ***/*****" ', required=True)
    parser.add_argument('-o', '--output', type=str, help='Output csv file with DOI\'s', required=True)
    
    args = parser.parse_args()
    
    DOI = args.input
    path = Path() / DOI
    if path.exists():
        data = []
        with open(path, "r") as f:
            lines = f.readlines()
            for id, doi in enumerate(lines):
                if not isDOI(doi):
                    print(f"{id+1}/{len(lines)} skiped bad DOI: {reference.doi}", end=clearLine)
                    continue
                
                print(f"{id+1}/{len(lines)} Recive info for input article. DOI: {doi}", end=clearLine)
                
                # for skip \r line
                print()
                article = CrossRefAPI.getArticleByDOI(doi)
                
                if article == Article():
                    print(f"Info not found on CrossRef for DOI: {DOI}")
                    continue
                
                for i, reference in enumerate(article.references):
                    print(f"{i+1}/{len(article.references)} Recive info for DOI: {reference.doi}", end=clearLine)
                    subArticle = CrossRefAPI.getArticleByDOI(reference.doi)
                    
                    if subArticle == Article():
                        print(f"Info not found on CrossRef for DOI: {DOI}")
                        continue
                
                    data.append((subArticle.doi, subArticle.title if subArticle.title != "no info" else subArticle.unstructured, subArticle.cited))

                # for skip \r line
                print()
                
            # for skip \r line
            print()
            
        # sort by count by the number of citations
        data.sort(key=lambda x: int(x[2]) if isinstance(x[2], int) else -1, reverse=True)
        
        print(f"Save output file to {args.output}")
        saveCSV(Path(args.output), data)
        
        return 0
    
    else:
        if isDOI(DOI):
            print(f"Recive info for input article. DOI: {DOI}")
            article = CrossRefAPI.getArticleByDOI(DOI)
            
            data = []
            
            if article == Article():
                print(f"Info not found on CrossRef for DOI: {DOI}")
                return 1
            
            for i, reference in enumerate(article.references):
                if not isDOI(reference.doi):
                    print(f"{i+1}/{len(article.references)} skiped bad DOI: {reference.doi}", end=clearLine)
                    continue
                
                print(f"{i+1}/{len(article.references)} Recive info for DOI: {reference.doi}", end=clearLine)
                
                subArticle = CrossRefAPI.getArticleByDOI(reference.doi)
                if subArticle == Article():
                    print(f"Info not found on CrossRef for DOI: {DOI}")
                    continue
                
                data.append((subArticle.doi, subArticle.title if subArticle.title != "no info" else subArticle.unstructured, subArticle.cited))
            
            # for skip \r line
            print()
            
            # sort by count by the number of citations
            data.sort(key=lambda x: x[2] if isinstance(x[2], int) else -1, reverse=True)
            
            print(f"Save output file to {args.output}")
            saveCSV(Path(args.output), data)
            
            return 0
                
        else:
            print(f"{path} is neither a file name nor a DOI")
            return 1


if __name__ == "__main__":
    main()