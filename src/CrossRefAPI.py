from Articles import Article, SimpleArticle
from datetime import datetime
from API import AbstractAPI
from requests import get

 

class CrossRefAPI(AbstractAPI):
    __root: str = "https://api.crossref.org"
    __routes: dict[str, str] = {
        "getArticle": "/works/"
    }
    
    @staticmethod
    def getArticleByDOI(DOI: str) -> Article:     
        responce = get(CrossRefAPI.__root + CrossRefAPI.__routes["getArticle"] + DOI)
        
        # setup default values
        title = "no info"
        publisher = "no info"
        date = datetime.fromtimestamp(0)
        cited = "no info"
        references = []
            
        if responce.status_code == 200:
            data = responce.json()
            
            # if successful response
            if data.get("status", False) == "ok":
                message = data.get("message", {})
                
                titles = message.get("title", [])
                title = titles[0] if len(titles) else "no info"
                
                publisher = message.get("publisher", "no info") 
                
                published = message.get("published", {"0": 1970, "1": 1, "2": 1})
                
                date = datetime(year=published.get("0", 1970),
                                month=published.get("1", 1),
                                day=published.get("2", 1))
                
                cited = message.get("is-referenced-by-count", -1)
                referencesList = message.get("reference", [])
                
                references = [SimpleArticle(item.get("DOI", "no info"), 
                                            item.get("unstructured", "no info")) 
                              for item in referencesList]            
            
        return Article(doi=DOI, title=title, unstructured=title, publisher=publisher, date=date, cited=cited, references=references)
    
    
    
if __name__ == "__main__":
    api = CrossRefAPI()
    print(api.getArticleByDOI("10.1038/nature12373"))