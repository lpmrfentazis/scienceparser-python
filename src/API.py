from Articles import Article, SimpleArticle
from datetime import datetime
from abc import ABC, abstractmethod, abstractproperty



class AbstractAPI(ABC):
    __root: str = ""
    __routes: dict[str, str] = {
        "getArticle": ""
    }
    
    @abstractmethod
    def getArticleByDOI(DOI: str) -> Article:
        return Article(doi="no info", 
                       title="no info", 
                       publisher="no info", 
                       date=datetime.fromtimestamp(0), 
                       cited=0, references=[])
